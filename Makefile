.DEFAULT_GOAL := show-ssh-config

request-confirmation:
	@echo -n "Are you sure you want to continue? [y/N] " && read ans && [ $${ans:-N} = y ]

install-confirmation:
	@echo "This should be a clean environment!"
	@echo "This will:"
	@echo "  Remove $(HOME)/.customize_environment if it exists"
	@echo "  Remove $(HOME)/bin if it exists"
	@echo "  Run $(PWD)/.customize_environment as root".
	@echo "  Create a symlink to $(PWD)/.customize_environment from $(HOME)/.customize_environment".
	@echo "  Place a symlink to $(PWD)/bin/cs in $(HOME)/bin. It will be named cs."
	@echo "  Place symlinks to whatever is installed via $(PWD)/.customize_environment in $(HOME)/bin."

clean-confirmation:
	@echo "This will:"
	@echo "  Remove $(HOME)\.customize_environment if it exists"
	@echo "  Remove $(HOME)\bin if it exists"
	@echo "You still need to restart Cloud Shell to have the software removed from outside your HOME directory"

show-ssh-config:
	@./bin/cs-show-ssh-config

do-clean:
	rm -f $(HOME)/.customize_environment
	rm -fr $(HOME)/bin

do-install:
	sudo $(PWD)/.customize_environment
	ln -s $(PWD)/.customize_environment $(HOME)/.customize_environment

install: install-confirmation request-confirmation do-clean do-install

clean: clean-confirmation request-confirmation do-clean

update:
	git pull

logs:
	less /var/log/customize_environment
