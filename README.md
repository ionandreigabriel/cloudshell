# CloudShell Setup

This is my setup for [Google Cloud Shell](https://cloud.google.com/shell/docs).

## How does it work

Good to know: [Google Cloud Shell: Environment Customization](https://cloud.google.com/shell/docs/configuring-cloud-shell#environment_customization)

1. A symlink is created from `$HOME/.customize_environment` to `$PWD/.customize_environment`
    * This triggers `$PWD/.customize_environment` execution on VM startup.
2. `$PWD/.customize_environment` cleans the environment by removing `$HOME/bin`.
    * `cs` is placed in `{HOME}/bin` as a symlink to `$PWD/bin/cs` - it just forwards all commands to `$PWD/make`.
        * This allows all `make` targets to be available from the moment the machine starts.
3. `$PWD/.customize_environment` runs all scripts in `$PWD/.customize_environment.d/*`
    * This triggers `$PWD/.customize_environment.d/*` execution on VM startup.
    * Allows for separation of concerns between various initializations and instalations.
    * Script are ran with `run-parts`, so be aware of the limitations!
4. Whatever is installed is installed either in `/opt/` or via `apt-get`.
    * This makes sure it does not take up from the 5GB `$HOME` directory.
5. All `$PWD/.customize_environment.d/*` scripts place symlinks in `$HOME/bin`
    * This makes sure the installed stuff becomes available to the logged in user as they get installed.

## Setup

### Prerequisites

A clean Google Cloud Shell environment. See [Reseting Cloud Shell](https://cloud.google.com/shell/docs/resetting-cloud-shell#resetting)

A login shell. $HOME/bin is only added to path when working in a login shell!
    * Get a login shell out of a non-login shell (e.g. when using cloudshell from a browser): `bash --login`.
    * Make sure VS Code launches the terminal with a login shell: [add "--login" to the `terminal.integrated.shellArgs.linux` list](https://code.visualstudio.com/docs/editor/integrated-terminal#_shell-arguments)

### Overview

1. Start Google Cloud Shell [from the console](https://cloud.google.com/shell/docs/launching-cloud-shell#launching_from_the_console).
2. Clone this repo.
3. Setup.

### Command line

```bash
git clone git@gitlab.com:ionandreigabriel/cloudshell.git
cd cloudshell
make install
```

### Cleanup

```bash
make clean
```

### Update

```bash
cs update
# Optional. If not done, it will get automatically updated on the next VM reset.
cs install
```

## Usefull targets

* `show-ssh-config`
  * Prints an entry which, when added to a remote `~/.ssh/config`, allows the remote to log in using `ssh cloudshell`. It assumes the existence of an identity file `~/.ssh/google_compute_engine` on the remote.
  * `~/.ssh/google_compute_engine` can be generated from cloudshell:

      ```bash
      gcloud alpha cloudshell ssh
      exit
      cloudshell download-files ~/.ssh/google-compute-engine ~/.ssh/google-compute-engine.pub
      rm ~/.ssh/google-compute-engine ~/.ssh/google-compute-engine.pub
      ```

* `logs`
  * Runs `less` on `/var/log/customize_environment`
  * One can get out of `less` using `:q`
  * One can follow contents of the file as they come along with `Shift-F`

## Usefull Links

### Generic

* [Using VSCode in portable mode](https://code.visualstudio.com/docs/editor/portable)
* [Remote - SSH VSCode Extension](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-ssh)
* [markdownlint VSCodeExtension](https://marketplace.visualstudio.com/items?itemName=DavidAnson.vscode-markdownlint)
* [CodeLLDB VSCode Extension](https://marketplace.visualstudio.com/items?itemName=vadimcn.vscode-lldb)

### Swift

* [SourceKit-LSP VSCode Extension](https://github.com/apple/sourcekit-lsp/tree/master/Editors/vscode)
